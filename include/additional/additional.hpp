#ifndef __ADDITIONAL_HPP__
#define __ADDITIONAL_HPP__

#include <glad/glad.h>

#include <cstdio>
#include <string>
#include <fstream>
#include <memory>
#include <iostream>

std::string cutPath(std::string const& pathThenFile) {
    std::string s_out = pathThenFile;
    std::size_t i;
    for(i = s_out.size() - 1; i && s_out[i] != '/' && s_out[i] != '\\'; --i);
    s_out.resize(++i);
    return(s_out);
}

#define LENINFOLOG static_cast<GLsizei>(512)

class ShaderProgram {
private:
    GLuint id;                  // this should be with const specifier or something like this
    static GLint success;       // declaration of static variable

public:
    ShaderProgram() { }

    bool init() {
        if(!(id = glCreateProgram())) {
            std::cerr << "\failed to create shader program\n" << std::endl;
            return(false);
        }
        return(true);
    }

    bool addShader(GLenum const& shaderType, std::string const& path) {
        /* read the file */
        std::ifstream ifs_file(path, std::ios::in | std::ios::binary);
        // if there will be troubles about the stream (file), we will be able to treat / check them
        // with stream flags (in particular, we check everything by calling ifs_file.bad() below)
        ifs_file.seekg(0, std::ios::end);
        std::streampos len = ifs_file.tellg();
        ifs_file.seekg(0, std::ios::beg);
        std::unique_ptr<GLchar> up_code;
        if(ifs_file.good()) {
            up_code.reset(new GLchar[static_cast<std::size_t>(len) + 1]);
        }
        ifs_file.read(up_code.get(), len); // it does no action if there were errors
        ifs_file.close();
        up_code.get()[static_cast<std::size_t>(len)] = '\0';
        if(!ifs_file.good()) {
            std::cerr << "\nfailed to read file:\n"
                      << path << '\n' << std::endl;
            return(false);
        }

        /* compile shader */
        GLuint shader = glCreateShader(shaderType);
        GLchar* p_code = up_code.get();
        glShaderSource(shader,
                       static_cast<GLsizei>(1),
                       const_cast<const GLchar* const*>(&p_code),
                       nullptr);
        glCompileShader(shader);
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if(!success) {
            GLchar infoLog[LENINFOLOG];
            glGetShaderInfoLog(shader, LENINFOLOG, nullptr, infoLog);
            std::string shaderTypeName;
            switch(shaderType) {
            case GL_VERTEX_SHADER:
                shaderTypeName = "vertex";
                break;
            case GL_GEOMETRY_SHADER:
                shaderTypeName = "geometry";
                break;
            case GL_FRAGMENT_SHADER:
                shaderTypeName = "fragment";
                break;
            }
            std::cerr << "\nfailed to compile " << shaderTypeName << " shader:\n"
                      << path << '\n'
                      << infoLog << std::endl;
            return(false);
        }

        /* other stuff */
        glAttachShader(id, shader); // attach shader to shader object
        glDeleteShader(shader); // mark to delete shader as soon as possible (when we need not it anymore)
        return(true);
    }

    bool link() {
        glLinkProgram(id);
        glGetProgramiv(id, GL_LINK_STATUS, &success);
        if(!success) {
            GLchar infoLog[LENINFOLOG];
            glGetProgramInfoLog(id, LENINFOLOG, nullptr, infoLog);
            std::cerr << "\nfailed to link shader program:\n"
                      << infoLog << std::endl;
            return(false);
        }
        return(true);
    }

    void use() {
        glUseProgram(id);
    }

    GLuint getID() {
        return(id);
    }
};

GLint ShaderProgram::success;        // definition of static variable

#undef LENINFOLOG

#endif//__ADDITIONAL_HPP__
