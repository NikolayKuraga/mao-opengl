CC = g++
CFLAGS = -g -O0 -Wall -Wextra -std=c++11 -pedantic -Wno-unused-parameter

EXEC = build/a.out

all:
	if [ ! -d build ]; then mkdir build; fi
	$(CC) $(CFLAGS) src/main.cpp src/glad.c -Iinclude/ -lglfw -lGL -lX11 -lpthread -lXrandr -lXi -ldl -lm -o $(EXEC)
	cp src/vertex_shader.glsl src/fragment_shader.glsl src/memory.jpg src/face.png build/

valgrind:
	valgrind $(EXEC)

clean:
	$(RM) -R build/*
	find . -name \*~ -delete
