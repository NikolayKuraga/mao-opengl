#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

#include <additional/additional.hpp>

#define RPATH_SHADER_VERTEX   "vertex_shader.glsl"
#define RPATH_SHADER_FRAGMENT "fragment_shader.glsl"
#define RPATH_TEXTURE1        "memory.jpg"
#define RPATH_TEXTURE2        "face.png"

#define WIN_WIDTH  800
#define WIN_HEIGHT 600

void framebuffer_size_callback(GLFWwindow* window, int width, int height); /* change framebuffer size */
void processInput(GLFWwindow* window); /* input control */

int main(int argc, char* argv[]) {
    /* initialize and configure GLFW */
    if(glfwInit() == GLFW_FALSE) {
        std::cerr << "failed to initialize GLFW\n" << std::endl;
        return(1);
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // something for Mac OS X
#endif//__APPLE__

    /* create GLFW window */
    GLFWwindow* window;
    if(!(window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, "Window title", NULL, NULL))) {
        std::cerr << "failed to create GLFW window\n" << std::endl;
        glfwTerminate();
        return(1);
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    /* load all OpenGL function pointers */
    if(!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cerr << "failed to initialize GLAD\n" << std::endl;
        glfwTerminate();
        return(1);
    }

    /* assemble shader program */
    std::string runDir = cutPath(argv[0]);
    ShaderProgram shaderProgram;
    if(!(shaderProgram.init() &&
         shaderProgram.addShader(GL_VERTEX_SHADER, runDir + RPATH_SHADER_VERTEX) &&
         shaderProgram.addShader(GL_FRAGMENT_SHADER, runDir + RPATH_SHADER_FRAGMENT) &&
         shaderProgram.link())) {
        glfwTerminate();
        return(1);
    }
    shaderProgram.use();

    /* define vertices */
    float vertices[] = { // positions -- x, y, z; colours -- r, g, b; texture coords -- s, t
        0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, // top right
        0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f, // bottom right
        -0.5f, -0.5f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom left
        -0.5f, 0.5f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f }; // top left
    unsigned int indices[] = {
        1, 2, 3,                // first triangle
        0, 1, 3 };              // second triangle

    /* setup buffers */
    GLuint VAO; glGenVertexArrays(1, &VAO);
    GLuint EBO; glGenBuffers(1, &EBO);
    GLuint VBO; glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 static_cast<GLsizeiptr>(sizeof(indices)),
                 static_cast<void*>(indices),
                 GL_STATIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER,
                 static_cast<GLsizeiptr>(sizeof(vertices)),
                 static_cast<void*>(vertices),
                 GL_STATIC_DRAW);
    glVertexAttribPointer(static_cast<GLuint>(0),
                          static_cast<GLint>(3),
                          GL_FLOAT,
                          GL_FALSE,
                          static_cast<GLsizei>(static_cast<std::size_t>(8u) * sizeof(float)),
                          reinterpret_cast<void*>(0u));
    glVertexAttribPointer(static_cast<GLuint>(1),
                          static_cast<GLint>(3),
                          GL_FLOAT,
                          GL_FALSE,
                          static_cast<GLsizei>(static_cast<std::size_t>(8u) * sizeof(float)),
                          reinterpret_cast<void*>(static_cast<std::size_t>(3u) * sizeof(float)));
    glVertexAttribPointer(static_cast<GLuint>(2),
                          static_cast<GLint>(2),
                          GL_FLOAT,
                          GL_FALSE,
                          static_cast<GLsizei>(static_cast<std::size_t>(8u) * sizeof(float)),
                          reinterpret_cast<void*>(static_cast<std::size_t>(6u) * sizeof(float)));
    glEnableVertexAttribArray(static_cast<GLuint>(0u));
    glEnableVertexAttribArray(static_cast<GLuint>(1u));
    glEnableVertexAttribArray(static_cast<GLuint>(2u));

    /* setup textures */    
    GLuint texture1, texture2;
    {
        int width, height, nrChannels;
        unsigned char* p_data_texture;
        std::string s_path_texture;
        stbi_set_flip_vertically_on_load(true); // helps to load pictures correctly (not mirrored)

        glGenTextures(1, &texture1);
        glBindTexture(GL_TEXTURE_2D, texture1);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        s_path_texture = runDir + RPATH_TEXTURE1;
        if(!(p_data_texture = stbi_load(s_path_texture.c_str(), &width, &height, &nrChannels, 0))) {
            std::cerr << "\nfailed to load texture:\n"
                      << s_path_texture << '\n' << std::endl;
            glfwTerminate();
            return(1);
        }
        glTexImage2D(GL_TEXTURE_2D,
                     static_cast<GLint>(0),
                     GL_RGB,
                     static_cast<GLsizei>(width),
                     static_cast<GLsizei>(height),
                     static_cast<GLint>(0),
                     GL_RGB,
                     GL_UNSIGNED_BYTE,
                     static_cast<void*>(p_data_texture));
        glGenerateMipmap(GL_TEXTURE_2D);
        stbi_image_free(p_data_texture);

        glGenTextures(1, &texture2);
        glBindTexture(GL_TEXTURE_2D, texture2);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
        s_path_texture = runDir + RPATH_TEXTURE2;
        if(!(p_data_texture = stbi_load(s_path_texture.c_str(), &width, &height, &nrChannels, 0))) {
            std::cerr << "\nfailed to load texture:\n"
                      << s_path_texture << '\n' << std::endl;
            glfwTerminate();
            return(1);
        }
        glTexImage2D(GL_TEXTURE_2D,
                     static_cast<GLint>(0),
                     GL_RGB,
                     static_cast<GLsizei>(width),
                     static_cast<GLsizei>(height),
                     static_cast<GLint>(0),
                     GL_RGBA,
                     GL_UNSIGNED_BYTE,
                     static_cast<void*>(p_data_texture));
        glGenerateMipmap(GL_TEXTURE_2D);
        stbi_image_free(p_data_texture);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture1);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);

        glUniform1i(glGetUniformLocation(shaderProgram.getID(), "texture1"), static_cast<GLint>(0));
        glUniform1i(glGetUniformLocation(shaderProgram.getID(), "texture2"), static_cast<GLint>(1));
    }

    while(!glfwWindowShouldClose(window)) {
        /* check input */
        processInput(window);

        /* render */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glDrawElements(GL_TRIANGLES,
                       static_cast<GLsizei>(6u),
                       GL_UNSIGNED_INT,
                       reinterpret_cast<void*>(0u));

        /* buffer swapping and register some IO events (pressing keys, moving mouse and so on) */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return(0);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, 1);
    }
}
